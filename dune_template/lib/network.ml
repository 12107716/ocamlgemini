open Base

type status =
  | SUCCESS (*20*)
  | WEIRD 

let code_to_status str =
  match str with
  | "20" -> SUCCESS
  | s -> failwith ("unknown status"^s)



(* get the first IP associated to the hostname *)
let get_ipaddress hostname =
  let open Lwt.Syntax in
  let* he = Lwt_unix.gethostbyname hostname in
  Lwt.return (Unix.string_of_inet_addr (he.h_addr_list).(0))


(* create the client socket *)
let create_socket () =
  let sock = Lwt_unix.socket PF_INET SOCK_STREAM 0 in
  Lwt_unix.set_blocking sock false;
  sock

(* create an SSL/TLS context *)
let sslctx = let () = Ssl.init () in
  Ssl.create_context Ssl.SSLv23 Ssl.Client_context 

(* connect to the capsule, dual way *)
let get_io_channels uri port =
  let open Lwt in
  let hoststring = Uri.host_with_default uri in
  Lwt_io.printf "URI is %s\n" hoststring >>= fun () -> 
  get_ipaddress hoststring  >>= fun ip ->
  Lwt_io.printf "IP is %s\n" ip >>= fun () -> 
  Lwt_unix.getaddrinfo ip port [] >>= fun addresses ->
  let server = Lwt_unix.((List.nth_exn addresses 0).ai_addr) in
  let socket = create_socket () in
  Lwt_unix.connect socket server >>= fun () ->
  Lwt_ssl.ssl_connect socket sslctx >>= fun socket ->
  let ic = Lwt_ssl.in_channel_of_descr socket in
  let oc = Lwt_ssl.out_channel_of_descr socket in
  return (ic, oc)

(* request an URI and print the raw text of the response *)
let get_and_print_raw uri (incoming, outgoing) =
  let open Lwt in
  let request = Printf.sprintf "%s\r\n" (Uri.to_string uri) in
  Lwt_io.write outgoing request >>= fun () ->
  Lwt_io.flush_all () >>= fun () ->
  Lwt_io.read_line incoming >>= fun head -> (* first line *)
  Lwt_io.fprintf Lwt_io.stdout "HEADER: %s\n" head >>= fun () ->
  Lwt_io.read incoming >>= fun raw -> (* rest of the answer *)
  Lwt_io.fprintf Lwt_io.stdout "RAW BODY: \n%s\n" raw


